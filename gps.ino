// This #include statement was automatically added by the Particle IDE.
#include "Particle.h"
#include "LIS3DH.h"
#include "AssetTrackerRK.h"
#include "application.h"
#include "cellular_hal.h"
#include "TinyGPS.h"

// System threading is required for this project
SYSTEM_THREAD(ENABLED);
FuelGauge batteryMonitor;
LIS3DHSPI accel(SPI, A2, WKP);
TinyGPSPlus gps;

struct MDM_CELL_LOCATE {
    int day;
    int month;
    int year;
    int hour;
    int minute;
    int second;
    char lat[14];
    char lng[14];
    int altitude; // only for GNSS positioning
    int uncertainty;
    int speed; // only for GNSS positioning
    int direction; // only for GNSS positioning
    int vertical_acc; // only for GNSS positioning
    int sensor_used; // 0: the last fix in the internal database, 2: CellLocate(R) location information
    int sv_used; // only for GNSS positioning
    int antenna_status; // only for GNSS positioning
    int jamming_status; // only for GNSS positioning
    int count;
    bool ok;
    int size;

    MDM_CELL_LOCATE()
    {
        memset(this, 0, sizeof(*this));
        size = sizeof(*this);
    }
};

MDM_CELL_LOCATE _cell_locate;
bool displayed_once = false;
volatile uint32_t cellTimeout;
volatile uint32_t cellTimeStart;
int retu; 
void displayInfo();

// This is the name of the Particle event to publish for battery or movement detection events
// It is a private event.
const char *eventName = "eBikeMonitor";

// Various timing constants
const unsigned long MAX_TIME_TO_PUBLISH_MS = 60000; // Only stay awake for 60 seconds trying to connect to the cloud and publish
const unsigned long MAX_TIME_FOR_GPS_FIX_MS = 5000; // Only stay awake for 5 seconds trying to get a GPS fix
const unsigned long TIME_AFTER_PUBLISH_MS = 4000; // After publish, wait 4 seconds for data to go out
const unsigned long TIME_AFTER_BOOT_MS = 5000; // At boot, wait 5 seconds before going to sleep again (after coming online)
const unsigned long TIME_PUBLISH_BATTERY_SEC = 15 * 60; // every 22 minutes send a battery update to keep the cellular connection up
const unsigned long SERIAL_PERIOD = 5000;


const uint8_t movementThreshold = 16;

// Stuff for the finite state machine
enum State { ONLINE_WAIT_STATE, RESET_STATE, RESET_WAIT_STATE, PUBLISH_STATE, MOBILE_STATE, SLEEP_STATE, SLEEP_WAIT_STATE, BOOT_WAIT_STATE, GPS_WAIT_STATE };
State state = ONLINE_WAIT_STATE;
unsigned long stateTime = 0;
int awake = 0;
unsigned long lastSerial = 0;
unsigned long startFix = 0;
bool gettingFix = false;
double Vref  = 3.3; //read your Vcc voltage,typical voltage should be 5000mV(5.0V)
int volt_pin = A0; 
double voltageValue; 
String id;

void handler(const char *topic, const char *data) {
   id = String (data);
}

void cell_locate_timeout_set(uint32_t timeout_ms) {
    cellTimeout = timeout_ms;
    cellTimeStart = millis();
}

bool is_cell_locate_timeout() {
    return (cellTimeout && ((millis()-cellTimeStart) > cellTimeout));
}

void cell_locate_timeout_clear() {
    cellTimeout = 0;
}

bool is_cell_locate_matched(MDM_CELL_LOCATE& loc) {
    return loc.ok;
}

/* Cell Locate Callback */
int _cbLOCATE(int type, const char* buf, int len, MDM_CELL_LOCATE* data)
{
    if ((type == TYPE_PLUS) && data) {
        // DEBUG CODE TO SEE EACH LINE PARSED
        // char line[256];
        // strncpy(line, buf, len);
        // line[len] = '\0';
        // Serial.printf("LINE: %s",line);

        // <response_type> = 1:
        //+UULOC: <date>,<time>,<lat>,<long>,<alt>,<uncertainty>,<speed>,<direction>,
        //        <vertical_acc>,<sensor_used>,<SV_used>,<antenna_status>,<jamming_status>
        //+UULOC: 25/09/2013,10:13:29.000,45.7140971,13.7409172,266,17,0,0,18,1,6,3,9
        int count = 0;
        //
        // TODO: %f was not working for float on LAT/LONG, so opted for capturing strings for now
        if ( (count = sscanf(buf, "\r\n+UULOC: %d/%d/%d,%d:%d:%d.%*d,%[^,],%[^,],%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n",
            &data->day,
            &data->month,
            &data->year,
            &data->hour,
            &data->minute,
            &data->second,
            data->lat,
            data->lng,
            &data->altitude,
            &data->uncertainty,
            &data->speed,
            &data->direction,
            &data->vertical_acc,
            &data->sensor_used,
            &data->sv_used,
            &data->antenna_status,
            &data->jamming_status) ) > 0 ) {
            // UULOC Matched
            data->count = count;
            data->ok = true;
        }
    }
    return WAIT;
}

int cell_locate(MDM_CELL_LOCATE& loc, uint32_t timeout_ms) {
    loc.count = 0;
    loc.ok = false;
    if (RESP_OK == Cellular.command(5000, "AT+ULOCCELL=0\r\n")) {
        if (RESP_OK == Cellular.command(_cbLOCATE, &loc, timeout_ms, "AT+ULOC=2,2,1,%d,5000\r\n", timeout_ms/1000)) {
            cell_locate_timeout_set(timeout_ms);
            if (loc.count > 0) {
                return loc.count;
            }
            return 0;
        }
        else {
            return -2;
            // Serial.println("Error! No Response from AT+LOC");
        }
    }
    // else Serial.println("Error! No Response from AT+ULOCCELL");
    return -1;
}

bool cell_locate_in_progress(MDM_CELL_LOCATE& loc) {
    if (!is_cell_locate_matched(loc) && !is_cell_locate_timeout()) {
        return true;
    }
    else {
        cell_locate_timeout_clear();
        return false;
    }
}

bool cell_locate_get_response(MDM_CELL_LOCATE& loc) {
    // Send empty string to check for URCs that were slow
    Cellular.command(_cbLOCATE, &loc, 1000, "");
    if (loc.count > 0) {
        return true;
    }
    return false;
}
void cell_locate_display(MDM_CELL_LOCATE& loc) {
    /* The whole kit-n-kaboodle */
    Serial.printlnf("\r\n%d/%d/%d,%d:%d:%d,LAT:%s,LONG:%s,%d,UNCERTAINTY:%d,SPEED:%d,%d,%d,%d,%d,%d,%d,MATCHED_COUNT:%d",
        loc.month,
        loc.day,
        loc.year,
        loc.hour,
        loc.minute,
        loc.second,
        loc.lat,
        loc.lng,
        loc.altitude,
        loc.uncertainty,
        loc.speed,
        loc.direction,
        loc.vertical_acc,
        loc.sensor_used,
        loc.sv_used,
        loc.antenna_status,
        loc.jamming_status,
        loc.count);

    /* A nice map URL */
    Serial.printlnf("\r\nhttps://www.google.com/maps?q=%s,%s\r\n",loc.lat,loc.lng);
    if (Particle.connected()) {
// The publish data contains 3 comma-separated values:
// whether movement was detected (1) or not (0) The not detected publish is used for battery status updates
// cell voltage (decimal)
// state of charge (decimal)
char data[64];
snprintf(data, sizeof(data), "%s,%s",
loc.lat, loc.lng);
            Particle.subscribe("particle/device/name", handler);  
            Particle.publish("particle/device/name");
            voltageValue = readVolt(volt_pin); 
            delay(4000); 
Particle.publish(eventName,
   String("{\"p\":\"") + id + "\","
                + "\"g\":\"" + data + "\","
                + "\"v\":\"" + String::format("%.2f",voltageValue) + "\"}",
                60, PRIVATE);
Serial.println(data);

// Wait for the publish to go out
stateTime = millis();
state = SLEEP_WAIT_STATE;
}
else {
// Haven't come online yet
if (millis() - stateTime >= MAX_TIME_TO_PUBLISH_MS) {
// Took too long to publish, just go to sleep
state = SLEEP_STATE;
}
}
}
double readVolt(int Pin)
{
    int analogValueArray[31];
    for(int index=0;index<31;index++)
    {
      analogValueArray[index]=analogRead(Pin);
    }
    int i,j,tempValue;
    for (j = 0; j < 31 - 1; j ++)
    {
        for (i = 0; i < 31 - 1 - j; i ++)
        {
            if (analogValueArray[i] > analogValueArray[i + 1])
            {
                tempValue = analogValueArray[i];
                analogValueArray[i] = analogValueArray[i + 1];
                analogValueArray[i + 1] = tempValue;
            }
        }
    }
    double medianValue = analogValueArray[(31 - 1) / 2];
    double voltValue = (medianValue / 4096.0 * Vref) *21;  //Sensitivity:100mV/A, 0A @ Vcc/2
    return voltValue;
}


void setup() {
//Serial.begin(9600);

// The GPS module on the AssetTracker is connected to Serial1 and D6
Serial1.begin(9600);

    pinMode(A0,INPUT);  
    pinMode(D6, OUTPUT);
    digitalWrite(D6, LOW);
    startFix = millis();
    gettingFix = true;
}


void loop() {
while (Serial1.available() > 0) {
if (gps.encode(Serial1.read())) {
displayInfo();
}
}

switch(state) {
case ONLINE_WAIT_STATE:
if (Particle.connected()) {
state = RESET_STATE;
}
if (millis() - stateTime > 5000) {
stateTime = millis();
Serial.println("waiting to come online");
}
break;

case RESET_STATE: {
Serial.println("resetting accelerometer");

LIS3DHConfig config;
config.setLowPowerWakeMode(16);

if (!accel.setup(config)) {
Serial.println("accelerometer not found");
state = SLEEP_STATE;
break;
}

state = BOOT_WAIT_STATE;
}
break;

case GPS_WAIT_STATE:
if (gps.location.isValid()) {
// Got a GPS fix
state = PUBLISH_STATE;
break;
}
if (millis() - stateTime >= MAX_TIME_FOR_GPS_FIX_MS) {
Serial.println("failed to get GPS fix");
state = MOBILE_STATE;
break;

}
break;
    case MOBILE_STATE:
        displayed_once = false;
        retu = cell_locate(_cell_locate, 60000);
            if (retu >= 8) {
                /* Got the response immediately */
                cell_locate_display(_cell_locate);
                displayed_once = true;
            }
            else if (retu == 0) {
                /* ret == 0, still waiting for the URC
                 * Check for cell locate response, and display it. */
                Serial.print("Waiting for URC ");
                while (cell_locate_in_progress(_cell_locate)) {
                    /* still waiting for URC */
                    if (cell_locate_get_response(_cell_locate)) {
                        cell_locate_display(_cell_locate);
                        displayed_once = true;
                    }
                    if (!displayed_once) Serial.print(".");
                }
                Serial.println();
                // Serial.println("Not in progress");

                /* We timed out, but maybe we have a response that includes LAT/LONG coords */
                if (!displayed_once && _cell_locate.count >= 8) {
                    cell_locate_display(_cell_locate);
                    displayed_once = true;
                }
            }
            else {
                /* ret == -1 */
                Serial.println("Cell Locate Error!");
                Particle.subscribe("particle/device/name", handler);  
                Particle.publish("particle/device/name");
                voltageValue = readVolt(volt_pin); 
                delay(4000); 
    Particle.publish(eventName,
       String("{\"p\":\"") + id + "\","
                    + "\"g\":\"" + "No Location" + "\","
                    + "\"v\":\"" + String::format("%.2f",voltageValue) + "\"}",
                    60, PRIVATE);
                state = SLEEP_STATE; 
            }
        break;
        
case PUBLISH_STATE:
if (Particle.connected()) {
// The publish data contains 3 comma-separated values:
// whether movement was detected (1) or not (0) The not detected publish is used for battery status updates
// cell voltage (decimal)
// state of charge (decimal)
char data[64];
snprintf(data, sizeof(data), "%f,%f",
gps.location.lat(), gps.location.lng());
            Particle.subscribe("particle/device/name", handler);  
            Particle.publish("particle/device/name");
            voltageValue = readVolt(volt_pin); 
            delay(4000); 
Particle.publish(eventName,
   String("{\"p\":\"") + id + "\","
                + "\"g\":\"" + data + "\","
                + "\"v\":\"" + String::format("%.2f",voltageValue) + "\"}",
                60, PRIVATE);
Serial.println(data);

// Wait for the publish to go out
stateTime = millis();
state = SLEEP_WAIT_STATE;
}
else {
// Haven't come online yet
if (millis() - stateTime >= MAX_TIME_TO_PUBLISH_MS) {
// Took too long to publish, just go to sleep
state = SLEEP_STATE;
}
}
break;

case SLEEP_WAIT_STATE:
if (millis() - stateTime >= TIME_AFTER_PUBLISH_MS) {
state = SLEEP_STATE;
}
break;

case BOOT_WAIT_STATE:
if (millis() - stateTime >= TIME_AFTER_BOOT_MS) {
// To publish the battery stats after boot, set state to PUBLISH_STATE
// To go to sleep immediately, set state to SLEEP_STATE
state = GPS_WAIT_STATE;
stateTime = millis();
}
break;

case SLEEP_STATE:
// Wait for Electron to stop moving for 2 seconds so we can recalibrate the accelerometer
accel.calibrateFilter(2000);

// Is this necessary?
   //digitalWrite(D6, HIGH);
   //pinMode(D6, INPUT);

   Serial.println("going to sleep");
   delay(500);

// Sleep
System.sleep(WKP, RISING, TIME_PUBLISH_BATTERY_SEC,SLEEP_NETWORK_STANDBY);

// This delay should not be necessary, but sometimes things don't seem to work right
// immediately coming out of sleep.
delay(500);

awake = ((accel.clearInterrupt() & LIS3DH::INT1_SRC_IA) != 0);

Serial.printlnf("awake=%d", awake);

// Restart the GPS
   //pinMode(D6, OUTPUT);
   digitalWrite(D6, LOW);
   startFix = millis();
   gettingFix = true;

state = GPS_WAIT_STATE;
stateTime = millis();
break;

}

}



void displayInfo()
{
if (millis() - lastSerial >= SERIAL_PERIOD) {
lastSerial = millis();

char buf[128];
if (gps.location.isValid()) {
snprintf(buf, sizeof(buf), "%f,%f", gps.location.lat(), gps.location.lng());
if (gettingFix) {
gettingFix = false;
unsigned long elapsed = millis() - startFix;
Serial.printlnf("%lu milliseconds to get GPS fix", elapsed);
}
}
else {
strcpy(buf, "no location");
if (!gettingFix) {
gettingFix = true;
startFix = millis();
}
}
Serial.println(buf);
}
}
